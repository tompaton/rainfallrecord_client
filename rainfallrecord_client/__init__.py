# coding: utf-8

import csv
from datetime import timedelta

import requests


class PostError(Exception):
    pass


class Location:

    RAIN = 'https://api.rainfallrecord.app/'

    def __init__(self, location, api_key):
        self.location = location
        self.headers = {'Authorization': 'Bearer {}'.format(api_key)}
        self.url = '{}locations/{}'.format(self.RAIN, self.location)

    @property
    def readings(self):
        return Readings(self._get_readings())

    def _get_readings(self):
        obj = requests.get(self.url + '.json', headers=self.headers).json()
        return obj['precipitation']

    def post(self, when, amount):
        res = requests.post(
            self.url + '/record.json',
            headers=self.headers,
            json={
                'utf8': '✓',
                'record': {
                    'date': when.strftime('%Y-%m-%d'),
                    'precipitation': amount
                }
            }
        )

        if res.status_code == 200:
            return True

        else:
            raise PostError(res.text)


class Readings:
    def __init__(self, readings):
        self.readings = readings

    @property
    def data(self):
        return self.readings

    def month_readings(self, when):
        return self.readings[str(when.year)][str(when.month)]

    def get_day_total(self, when):
        try:
            reading = float(self.month_readings(when)[str(when.day)])
        except KeyError:
            reading = 0
        return [when.strftime('%d %b'), reading]

    def get_month_total(self, when):
        try:
            total = sum(float(i)
                        for i in list(self.month_readings(when).values()))
        except KeyError:
            total = 0
        return [when.strftime('%b'), total]

    def get_week_total(self, when):
        start = when - timedelta(days=when.weekday())
        return sum([self.get_day_total(d)[1]
                    for d in (start + timedelta(days=i)
                              for i in range(0, 7))])
