# coding: utf-8

from datetime import datetime
from unittest.mock import Mock, patch

import pytest

from rainfallrecord_client import Location, Readings, PostError
import rainfallrecord_client as client


TEST_API_KEY = 'token1'
TEST_HEADERS = {'Authorization': 'Bearer {}'.format(TEST_API_KEY)}


def test_url():
    assert Location(123, TEST_API_KEY).url \
        == 'https://api.rainfallrecord.app/locations/123'


@patch.object(client, 'requests')
def test_post(requests):
    when = datetime(2017, 8, 27, 8, 0, 0)

    result = Mock()
    result.status_code = 200
    requests.post.return_value = result

    assert Location(123, TEST_API_KEY).post(when, 12.3) == True

    requests.post.assert_called_with(
        'https://api.rainfallrecord.app/locations/123/record.json',
        headers=TEST_HEADERS,
        json={
            'utf8': '✓',
            'record': {
                'date': '2017-08-27',
                'precipitation': 12.3
            }
        }
    )


@patch.object(client, 'requests')
def test_rain_basic_day_error(requests):
    when = datetime(2017, 8, 27, 8, 0, 0)

    result = Mock()
    result.status_code = 500
    result.text = 'error!'
    requests.post.return_value = result

    with pytest.raises(PostError) as ex:
        Location(123, TEST_API_KEY).post(when, 12.3)

    assert str(ex.value) == 'error!'

    requests.post.assert_called_with(
        'https://api.rainfallrecord.app/locations/123/record.json',
        headers=TEST_HEADERS,
        json={
            'utf8': '✓',
            'record': {
                'date': '2017-08-27',
                'precipitation': 12.3
            }
        }
    )

@patch.object(client, 'requests')
def test_get_readings(requests, location_json, location_data):
    requests.get.return_value = location_json

    assert Location(123, TEST_API_KEY).readings.data == location_data

    requests.get.assert_called_with(
        'https://api.rainfallrecord.app/locations/123.json',
        headers=TEST_HEADERS
    )


def test_get_day_total(location_data):
    when = datetime(2017, 8, 27, 8, 0, 0)
    assert Readings(location_data).get_day_total(when) == ['27 Aug', 14.0]


def test_get_month_total(location_data):
    when = datetime(2017, 8, 27, 8, 0, 0)
    assert Readings(location_data).get_month_total(when) == ['Aug', 88.5]


def test_get_week_total(location_data):
    when = datetime(2017, 8, 27, 8, 0, 0)
    assert Readings(location_data).get_week_total(when) == 29.5


def test_get_week_total_month_span(location_data):
    when = datetime(2017, 7, 31, 8, 0, 0)
    assert Readings(location_data).get_week_total(when) == 15.0


@pytest.fixture
def location_json(location_data):
    result = Mock()
    def json():
        return {
            "id": 3,
            "title": "Testing",
            "street_address": "",
            "town_suburb": "Melbourne",
            "region": "VIC",
            "country": "Australia",
            "latitude": None,
            "longitude": None,
            "precipitation": location_data
        }
    result.json = json
    return result


@pytest.fixture
def location_data():
    return {
        "2017": {
            "7": {
                "19": 8.5,
                "21": 4.5,
                "23": 8.5,
                "24": 1.5,
                "27": 4,
                "29": 6
            },
            "8": {
                "3": 1,
                "4": 9.5,
                "5": 1,
                "6": 3.5,
                "7": 4,
                "8": 1,
                "11": 1,
                "12": 4,
                "13": 4,
                "15": 2,
                "16": 5,
                "17": 9,
                "19": 14,
                "21": 0,
                "22": 3,
                "23": 9,
                "24": 3.5,
                "25": 0,
                "27": 14
            }
        },
    }
